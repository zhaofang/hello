# cordova 初始化项目
###### web项目地址： https://gitlab.com/zhaofang/hackernews7
#### 1. 初始化 
**```cordova create <path> <app id> <name>```**
```js
cordova create hello com.example.hello HelloWorld
```
#### 2. 添加android平台
``` cordova platform android```
#### 3. 添加ios平台(本项目未添加)
``` cordova platform ios```
#### 4. 添加自动更新插件
``` cordova plugin add cordova-hot-code-push-plugin```
添加cordova hot code push客户端，用于生成www目录下文件的hash码，更新的时候对比使用.
#### 5. 全局安装
``` npm install -g cordova-hot-code-push-cli```
### 客户端配置(cordova生成的项目)
#### 1. 修改config.xml
```js
    <chcp>
        <auto-download enabled="false" />
        <auto-install enabled="false" />
        <config-file url="http:192.168.3.89:3000" />
    </chcp>
```
#### 2. 如修改代码或新安装插件重新打包，把www目录复制到根目录，执行```cordova prepare android```
#### 3. 浏览器调试控制台输入: ```location.href = "http:192.168.3.89:3000"```,web项目热更新后可实时调试代码，需如下配置:
cordova 生成的android app默认情况下webview中的超链接，如果不是相对路径，会默认使用浏览器打开。解决方案：修改config.xml文件添加链接配置节点
```js
<!--配置WebView可以打开的外部链接-->
  <allow-navigation href="http://*/*" />
```
### 服务器端(web 项目)
#### 1. 项目根目录新建cordova-hcp.json
```js
{
  "update": "resume",
  "content_url": "http://192.168.3.89:3000"
}
```
#### 2. 执行```cordova-hcp build```,生成hash文件
#### 3. 把www目录放在config-file中的目录，本项目为本地.